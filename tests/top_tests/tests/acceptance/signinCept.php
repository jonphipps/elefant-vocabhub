<?php

$I = new WebGuy($scenario);
$U = new UserController($I);

$U->logout();

$I->wantTo('login to site as an admin');
$I->click(HomePage::$adminLink);
$U->loginAdmin(adminUser::$email, adminUser::$password);

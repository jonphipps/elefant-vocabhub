<?php
/**
 * Created by jphipps, on 5/21/13 at 1:52 PM
 * for the elefant-vocabhub project
 */

$I     = new WebGuy($scenario);
$U     = new UserController($I);
$email = new EmailController();
if ($scenario->running()) {
	$U->logout();
}
$I->wantTo('signup as a new customer');
$I->amOnPage(SignupPage::$URL);
$I->see(SignupPage::$title, "h1");
$I->fillField(SignupPage::$nameInput, fooAdmin::$name);
$I->fillField(SignupPage::$emailInput, fooAdmin::$email);
$I->fillField(SignupPage::$passwordInput, fooAdmin::$password);
$I->fillField(SignupPage::$password2Input, fooAdmin::$password);
$I->fillField(SignupPage::$companyInput, fooAdmin::$company);
$I->fillField(SignupPage::$subdomainInput, fooAdmin::$domain);
$I->checkOption(SignupPage::$publicInput);
$I->click(SignupPage::$submitButton);
//$I->amGoingTo("verify that I have received the signup email");
$I->seeInDatabase(
	DbController::$accountTable,
	array(
	     "id"       => 4,
	     "user"     => 2,
	     "customer" => 3,
	     "type"     => "owner",
	     "enabled"  => 1
	)
);
$I->seeInDatabase(
	DbController::$customerTable,
	array(
	     "id"        => 3,
	     "name"      => "Foo Enterprises",
	     "subdomain" => "foo",
	     "public"    => 1,
	     "level"     => 1
	)
);
if ($scenario->running()) {
	$email->openLatestMailerFile(DbController::$projectDir . "cache/mailer");
}
$I->seeInDatabase(
	DbController::$userTable,
	array(
	     "id"       => 2,
	     "email"    => "jonphipps+fooAdmin@gmail.com",
	     "name"     => "Jon FooAdmin",
	     "type"     => "member",
	     "userdata" => $email->dbUserData
	)
);
//$I->amGoingTo("check that the verification link logs me in");
$I->amOnSubdomain("foo");
$I->amOnPage("/");
$I->see("Foo Enterprises");
$I->see("Dashboard");
$I->amOnPage($email->getVerifyLinkFromEmail());
$I->see("Account Verified");
$I->click("Continue");
$I->seeInCurrentUrl("/omr/account");
$I->seeInDatabase(
	DbController::$userTable,
	array(
	     "id"       => 2,
	     "email"    => "jonphipps+fooAdmin@gmail.com",
	     "name"     => "Jon FooAdmin",
	     "type"     => "member",
	     "userdata" => "[]"
	)
);

//$I->amGoingTo("invite a new member");
$I->click("Add a member");
$I->see("Add Member");
$I->fillField("add-member-name", FooMember::$name);
$I->fillField("add-member-email", FooMember::$email);
$I->click("Add member");

//$I->amGoingTo("check the existence of the invitation email");
if ($scenario->running()) {
	$I->wait(2000);
	$email->openLatestMailerFile(DbController::$projectDir . "cache/mailer");
	$email->getInviteCredentialsFromEmail();
}
//$I->amGoingTo("check the newly invited user's ability to login");
if ($scenario->running()) {
//xdebug_break();
	$U->logout();
}
$I->amOnSubdomain("foo");
$I->amOnPage(FooUserLoginPage::$URL);
$I->see(FooUserLoginPage::$title);
$I->seeInCurrentUrl(FooUserLoginPage::$URL);
//$I->wait(10000);

$I->fillField(FooUserLoginPage::$emailInput, $email->inviteLogin);
$I->fillField(FooUserLoginPage::$passwordInput, $email->invitePassword);
$I->click(FooUserLoginPage::$submitButton);
//$I->wait(10000);
$I->see("Account");
$I->seeInField("name",FooMember::$name);
$I->seeInField("email",FooMember::$email);

//$I->amGoingTo("make sure they can get to the account page to change their password");
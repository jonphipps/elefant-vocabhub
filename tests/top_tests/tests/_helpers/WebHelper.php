<?php
namespace Codeception\Module;

// here you can define custom functions for WebGuy

class WebHelper extends Filesystem {

	protected $verifyLink;

	public function openLatestMailerFile($path) {
		/** @var Filesystem $filesystem */
		$latest_ctime    = 0;
		$latest_filename = '';

		$d = dir($path);
		while (false !== ($entry = $d->read())) {
			$filepath = "{$path}/{$entry}";
			// could do also other checks than just checking whether the entry is a file
			if (is_file($filepath) && filectime($filepath) > $latest_ctime) {
				$latest_ctime    = filectime($filepath);
				$latest_filename = $entry;
			}
		}

		$file = $path . DIRECTORY_SEPARATOR . $latest_filename;
		//$filesystem->seeFileFound($file);
		//$foo = file_get_contents($file);
		//print_r($foo);
		$this->filepath = $file;
		$this->openFile($file);
		$this->assertContains("http", $this->file);
	}

	public function getVerifyLinkFromEmail() {
		//find the link, plus a bit of surrounding text
		$pattern = array('/(?mi-Us)\\R/', '/(?mi-Us)=\r/', '/=3D/', '|(?mi-Us).*(http://.*)=0A=0AT.*|');
		$replace = array("\r", "", "=" , "$1");
		//strip the newlines and extract the link
		$link = preg_replace($pattern, $replace, $this->file);
		$this->verifyLink = $link;
		$this->debug($link);
		return $link;

		/** @var PhpBrowser $browser */
		//$browser = $this->getModule("PhpBrowser");
		//$browser-amOnPage($link);

	}
}

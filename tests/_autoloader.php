<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jphipps
 * Date: 5/8/13
 * Time: 3:20 PM
 * To change this template use File | Settings | File Templates.
 */
spl_autoload_register(function ($className) {
		foreach (array('PageObjects', 'Controllers') as $type) {
			$filePath = __DIR__ . DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR . $className . '.php';
			if (file_exists($filePath)) {
				include_once($filePath);
			}
		}
	});


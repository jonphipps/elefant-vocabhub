<?php
$I = new WebGuy($scenario);
$U = new UserController($I);

$I->wantTo('login to the foo dashboard from the login screen');
$I->amOnPage("/user/login");
$I->see("Foo");
$I->see("Members", "h1");
$U->loginFooAdmin(FooAdmin::$email, FooAdmin::$password);
$I->see("Account", "h1");

$U->logout();
$I->see(FooHomePage::$title);

$I->wantTo('login to the private foo subdomain');
$I->amOnPage("");
$I->see(FooHomePage::$title);
$I->see("Email");


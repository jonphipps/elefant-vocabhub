-- MySQL dump 10.14  Distrib 5.5.30-MariaDB, for osx10.7 (i386)
--
-- Host: localhost    Database: elefant_demo
-- ------------------------------------------------------
-- Server version	5.2.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` char(72) NOT NULL,
  `user` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `ts` datetime NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `identifier` (`identifier`,`status`,`ts`),
  KEY `user` (`user`,`status`,`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_api`
--

DROP TABLE IF EXISTS `elefant_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_api` (
  `token` char(35) NOT NULL,
  `api_key` char(35) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`token`),
  KEY `token` (`token`,`api_key`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_api`
--

LOCK TABLES `elefant_api` WRITE;
/*!40000 ALTER TABLE `elefant_api` DISABLE KEYS */;
/*!40000 ALTER TABLE `elefant_api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_apps`
--

DROP TABLE IF EXISTS `elefant_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_apps` (
  `name` char(48) NOT NULL,
  `version` char(16) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_apps`
--

LOCK TABLES `elefant_apps` WRITE;
/*!40000 ALTER TABLE `elefant_apps` DISABLE KEYS */;
INSERT INTO `elefant_apps` VALUES ('blog','1.1.3-stable'),('comments','0.9-beta'),('filemanager','1.3.0-beta'),('form','1.0.4-stable'),('saasy','0.9.0-beta'),('user','1.1.3-stable'),('wiki','0.9.1-beta');
/*!40000 ALTER TABLE `elefant_apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_block`
--

DROP TABLE IF EXISTS `elefant_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_block` (
  `id` char(72) NOT NULL,
  `title` char(72) NOT NULL,
  `body` text,
  `access` enum('public','member','private') NOT NULL DEFAULT 'public',
  `show_title` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`),
  KEY `access` (`access`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_block`
--

LOCK TABLES `elefant_block` WRITE;
/*!40000 ALTER TABLE `elefant_block` DISABLE KEYS */;
INSERT INTO `elefant_block` VALUES ('members','Members','{! user/sidebar !}','public','no');
/*!40000 ALTER TABLE `elefant_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_blog_post`
--

DROP TABLE IF EXISTS `elefant_blog_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(72) NOT NULL,
  `ts` datetime NOT NULL,
  `author` char(32) NOT NULL,
  `published` enum('yes','no') NOT NULL,
  `body` text NOT NULL,
  `tags` text NOT NULL,
  `extra` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ts` (`ts`),
  KEY `ts_2` (`ts`,`published`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_blog_post`
--

LOCK TABLES `elefant_blog_post` WRITE;
/*!40000 ALTER TABLE `elefant_blog_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `elefant_blog_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_blog_post_tag`
--

DROP TABLE IF EXISTS `elefant_blog_post_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_blog_post_tag` (
  `tag_id` char(24) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`tag_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_blog_post_tag`
--

LOCK TABLES `elefant_blog_post_tag` WRITE;
/*!40000 ALTER TABLE `elefant_blog_post_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `elefant_blog_post_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_blog_tag`
--

DROP TABLE IF EXISTS `elefant_blog_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_blog_tag` (
  `id` char(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_blog_tag`
--

LOCK TABLES `elefant_blog_tag` WRITE;
/*!40000 ALTER TABLE `elefant_blog_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `elefant_blog_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_extended_fields`
--

DROP TABLE IF EXISTS `elefant_extended_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_extended_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` char(48) NOT NULL,
  `sort` int(11) NOT NULL,
  `name` char(48) NOT NULL,
  `label` char(48) NOT NULL,
  `type` char(24) NOT NULL,
  `required` int(11) NOT NULL,
  `options` char(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class` (`class`,`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_extended_fields`
--

LOCK TABLES `elefant_extended_fields` WRITE;
/*!40000 ALTER TABLE `elefant_extended_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `elefant_extended_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_filemanager_prop`
--

DROP TABLE IF EXISTS `elefant_filemanager_prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_filemanager_prop` (
  `file` char(128) NOT NULL,
  `prop` char(32) NOT NULL,
  `value` char(255) NOT NULL,
  PRIMARY KEY (`file`),
  KEY `prop` (`prop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_filemanager_prop`
--

LOCK TABLES `elefant_filemanager_prop` WRITE;
/*!40000 ALTER TABLE `elefant_filemanager_prop` DISABLE KEYS */;
/*!40000 ALTER TABLE `elefant_filemanager_prop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_form`
--

DROP TABLE IF EXISTS `elefant_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(48) NOT NULL,
  `message` text NOT NULL,
  `ts` datetime NOT NULL,
  `fields` text NOT NULL,
  `actions` text NOT NULL,
  `response_title` char(48) NOT NULL,
  `response_body` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `ts` (`ts`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_form`
--

LOCK TABLES `elefant_form` WRITE;
/*!40000 ALTER TABLE `elefant_form` DISABLE KEYS */;
INSERT INTO `elefant_form` VALUES (1,'Untitled','Please fill in the following information.','2013-04-03 23:29:36','[]','[]','Thank you','Your information has been saved.');
/*!40000 ALTER TABLE `elefant_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_form_results`
--

DROP TABLE IF EXISTS `elefant_form_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_form_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `ts` datetime NOT NULL,
  `ip` char(15) NOT NULL,
  `results` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`,`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_form_results`
--

LOCK TABLES `elefant_form_results` WRITE;
/*!40000 ALTER TABLE `elefant_form_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `elefant_form_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_lock`
--

DROP TABLE IF EXISTS `elefant_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_lock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `resource` varchar(72) NOT NULL,
  `resource_id` varchar(72) NOT NULL,
  `expires` datetime NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `resource` (`resource`,`resource_id`,`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_lock`
--

LOCK TABLES `elefant_lock` WRITE;
/*!40000 ALTER TABLE `elefant_lock` DISABLE KEYS */;
INSERT INTO `elefant_lock` VALUES (2,1,'Designer','layouts/admin.html','2013-04-03 21:40:13','2013-04-03 21:00:13','2013-04-03 21:00:13'),(3,1,'Webpage','index','2013-05-21 14:52:30','2013-05-21 14:12:30','2013-05-21 14:12:30');
/*!40000 ALTER TABLE `elefant_lock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_saasy_acct`
--

DROP TABLE IF EXISTS `elefant_saasy_acct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_saasy_acct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `type` char(12) NOT NULL,
  `enabled` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`,`customer`),
  KEY `customer` (`customer`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_saasy_acct`
--

LOCK TABLES `elefant_saasy_acct` WRITE;
/*!40000 ALTER TABLE `elefant_saasy_acct` DISABLE KEYS */;
INSERT INTO `elefant_saasy_acct` VALUES (4,2,3,'owner',1);
/*!40000 ALTER TABLE `elefant_saasy_acct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_saasy_customer`
--

DROP TABLE IF EXISTS `elefant_saasy_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_saasy_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(72) NOT NULL,
  `subdomain` char(72) NOT NULL,
  `level` int(11) NOT NULL,
  `public` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subdomain` (`subdomain`),
  KEY `level` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_saasy_customer`
--

LOCK TABLES `elefant_saasy_customer` WRITE;
/*!40000 ALTER TABLE `elefant_saasy_customer` DISABLE KEYS */;
INSERT INTO `elefant_saasy_customer` VALUES (3,'Foo Enterprises','foo',1,1);
/*!40000 ALTER TABLE `elefant_saasy_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_user`
--

DROP TABLE IF EXISTS `elefant_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` char(72) NOT NULL,
  `password` char(128) NOT NULL,
  `session_id` char(32) DEFAULT NULL,
  `expires` datetime NOT NULL,
  `name` char(72) NOT NULL,
  `type` char(32) NOT NULL,
  `signed_up` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `userdata` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `session_id` (`session_id`),
  KEY `email_2` (`email`,`password`),
  KEY `session_id_2` (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_user`
--

LOCK TABLES `elefant_user` WRITE;
/*!40000 ALTER TABLE `elefant_user` DISABLE KEYS */;
INSERT INTO `elefant_user` VALUES (1,'Jonphipps@gmail.com','$2a$07$jIbRk3iq212VgMd9xhIAKOVfpKM869iG9a.VsZPS/ayMmNJMbsfCO','4a94dd6024645f5f0939848f885aa192','2013-06-20 19:05:07','Jon Phipps','admin','2013-04-10 15:14:02','2013-04-10 15:14:02','[]'),(2,'jonphipps+fooAdmin@gmail.com','$2a$07$6t2YFXOQF2I7Ve7pmtY6JeufY0jUL9jszTNXoQKttHoiMt9r4xe1i','e80fbe86a784b4145e298e3abb32d793','2013-06-22 18:04:09','Jon FooAdmin','member','2013-05-23 18:04:09','2013-05-23 18:04:09','{\"verifier\":\"a61e56591fe2c25e6465f449acc347bd\"}');
/*!40000 ALTER TABLE `elefant_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_user_openid`
--

DROP TABLE IF EXISTS `elefant_user_openid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_user_openid` (
  `token` char(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_user_openid`
--

LOCK TABLES `elefant_user_openid` WRITE;
/*!40000 ALTER TABLE `elefant_user_openid` DISABLE KEYS */;
/*!40000 ALTER TABLE `elefant_user_openid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_versions`
--

DROP TABLE IF EXISTS `elefant_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` char(72) NOT NULL,
  `pkey` char(72) NOT NULL,
  `user` int(11) NOT NULL,
  `ts` datetime NOT NULL,
  `serialized` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class` (`class`,`pkey`,`ts`),
  KEY `user` (`user`,`ts`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_versions`
--

LOCK TABLES `elefant_versions` WRITE;
/*!40000 ALTER TABLE `elefant_versions` DISABLE KEYS */;
INSERT INTO `elefant_versions` VALUES (1,'Webpage','index',0,'2013-04-10 15:14:02','{\"id\":\"index\",\"title\":\"Welcome to Elefant\",\"menu_title\":\"Home\",\"window_title\":\"\",\"access\":\"public\",\"layout\":\"default\",\"description\":\"\",\"keywords\":\"\",\"body\":\"<table><tbody><tr><td><h3>Congratulations!</h3>You have successfully installed Elefant, the refreshingly simple new PHP web framework and CMS.</td><td><h3>Getting Started</h3>To log in as an administrator and edit pages, write a blog post, or upload files, go to <a href=\"/admin\">/admin</a>.</td><td><h3>Developers</h3>Documentation, source code and issue tracking can be found at <a href=\"http://github.com/jbroadway/elefant\">github.com/jbroadway/elefant</a></td></tr></tbody></table>\"}'),(2,'Block','members',0,'2013-04-10 15:14:02','{\"id\":\"members\",\"title\":\"Members\",\"body\":\"{! user/sidebar !}\",\"access\":\"public\",\"show_title\":\"no\"}'),(3,'User','2',0,'2013-04-10 15:36:55','{\"name\":\"Joe Blough\",\"email\":\"jonphipps+joe@gmail.com\",\"password\":\"$2a$07$fo7HQE2n1BwKH2uUDy69Fer08SRoNplC3P7bhUqcJeKB2q48A24Q2\",\"expires\":\"2013-04-10 15:36:55\",\"type\":\"member\",\"signed_up\":\"2013-04-10 15:36:55\",\"updated\":\"2013-04-10 15:36:55\",\"userdata\":\"{\"verifier\":\"ffe295ffb54b359463edbb16fba1f869\"}\",\"id\":\"2\"}'),(4,'User','3',2,'2013-04-10 16:14:05','{\"name\":\"Jose Blough\",\"email\":\"jonphipps+jose@gmail.com\",\"password\":\"$2a$07$hstRDwG9wLkKkTZo1eKVEOGewSC6E/9PnSjrM4IAiWWJkHdr6D.0e\",\"expires\":\"2013-04-10 16:14:05\",\"type\":\"member\",\"signed_up\":\"2013-04-10 16:14:05\",\"updated\":\"2013-04-10 16:14:05\",\"userdata\":\"[]\",\"id\":\"3\"}'),(5,'User','4',0,'2013-04-12 14:48:10','{\"name\":\"Bing Pan\",\"email\":\"jonphipps+bing@gmail.com\",\"password\":\"$2a$07$XN4DIOhpDYcy4fubw5Nczu0bw/p0iDVaf1JbHxYb09AdAkG24XsHq\",\"expires\":\"2013-04-12 14:48:10\",\"type\":\"member\",\"signed_up\":\"2013-04-12 14:48:10\",\"updated\":\"2013-04-12 14:48:10\",\"userdata\":\"{\"verifier\":\"925d70cebb10cdc7902cffe9f9d5991b\"}\",\"id\":\"4\"}'),(6,'User','4',0,'2013-04-12 14:51:49','{\"name\":\"Bing Pan\",\"email\":\"jonphipps+bing@gmail.com\",\"password\":\"$2a$07$kv5ezEsWtwuLlprVGdRU6.SPS5TTKttK3LNnGG2VRVj0qfMI2Krzm\",\"expires\":\"2013-04-12 14:51:49\",\"type\":\"member\",\"signed_up\":\"2013-04-12 14:51:49\",\"updated\":\"2013-04-12 14:51:49\",\"userdata\":\"{\"verifier\":\"5b9b88abfed828ab3f5a43334ef6503c\"}\",\"id\":\"4\"}'),(7,'Webpage','index',1,'2013-04-28 16:33:31','{\"id\":\"index\",\"title\":\"Welcome to Elefant\",\"menu_title\":\"Home\",\"window_title\":\"\",\"access\":\"public\",\"layout\":\"default\",\"description\":\"\",\"keywords\":\"\",\"body\":\"<table><tbody><tr><td><h3>Congratulations!<\\/h3>You have successfully installed Elefant, the refreshingly simple new PHP web framework and CMS.<\\/td><td><h3>Getting Started<\\/h3>To log in as an administrator and edit pages, write a blog post, or upload files, go to <a id=\\\"link-admin\\\" href=\\\"\\/admin\\\">\\/admin<\\/a>.<\\/td><td><h3>Developers<\\/h3>Documentation, source code and issue tracking can be found at <a href=\\\"http:\\/\\/github.com\\/jbroadway\\/elefant\\\">github.com\\/jbroadway\\/elefant<\\/a><\\/td><\\/tr><\\/tbody><\\/table>\\r\\n\"}'),(8,'User','1',1,'2013-05-07 21:11:17','{\"id\":\"1\",\"email\":\"jphipps@madcreek.com\",\"password\":\"$2a$07$jIbRk3iq212VgMd9xhIAKOVfpKM869iG9a.VsZPS\\/ayMmNJMbsfCO\",\"session_id\":\"bd9f8560be758802a7d71edde2f56db1\",\"expires\":\"2013-06-06 21:07:49\",\"name\":\"Jon Phipps\",\"type\":\"admin\",\"signed_up\":\"2013-04-10 15:14:02\",\"updated\":\"2013-04-10 15:14:02\",\"userdata\":\"[]\"}'),(9,'User','1',1,'2013-05-07 21:12:10','{\"id\":\"1\",\"email\":\"jonphipps@gmail.com\",\"password\":\"$2a$07$jIbRk3iq212VgMd9xhIAKOVfpKM869iG9a.VsZPS\\/ayMmNJMbsfCO\",\"session_id\":\"bd9f8560be758802a7d71edde2f56db1\",\"expires\":\"2013-06-06 21:07:49\",\"name\":\"Jon Phipps\",\"type\":\"admin\",\"signed_up\":\"2013-04-10 15:14:02\",\"updated\":\"2013-04-10 15:14:02\",\"userdata\":\"[]\"}'),(10,'User','1',1,'2013-05-07 21:15:53','{\"id\":\"1\",\"email\":\"Jonphipps@gmail.com\",\"password\":\"$2a$07$jIbRk3iq212VgMd9xhIAKOVfpKM869iG9a.VsZPS\\/ayMmNJMbsfCO\",\"session_id\":\"796310f54c9ab7bc90be37f5d6b67ec9\",\"expires\":\"2013-06-06 21:15:14\",\"name\":\"Jon Phipps\",\"type\":\"admin\",\"signed_up\":\"2013-04-10 15:14:02\",\"updated\":\"2013-04-10 15:14:02\",\"userdata\":\"[]\"}'),(11,'User','2',0,'2013-05-23 18:04:09','{\"name\":\"Jon FooAdmin\",\"email\":\"jonphipps+fooAdmin@gmail.com\",\"password\":\"$2a$07$6t2YFXOQF2I7Ve7pmtY6JeufY0jUL9jszTNXoQKttHoiMt9r4xe1i\",\"expires\":\"2013-05-23 18:04:09\",\"type\":\"member\",\"signed_up\":\"2013-05-23 18:04:09\",\"updated\":\"2013-05-23 18:04:09\",\"userdata\":\"{\\\"verifier\\\":\\\"a61e56591fe2c25e6465f449acc347bd\\\"}\",\"id\":\"2\"}');
/*!40000 ALTER TABLE `elefant_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elefant_webpage`
--

DROP TABLE IF EXISTS `elefant_webpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elefant_webpage` (
  `id` char(72) NOT NULL,
  `title` char(72) NOT NULL,
  `menu_title` char(72) NOT NULL,
  `window_title` char(72) NOT NULL,
  `access` enum('public','member','private') NOT NULL DEFAULT 'public',
  `layout` char(48) NOT NULL,
  `description` text,
  `keywords` text,
  `body` text,
  PRIMARY KEY (`id`),
  KEY `access` (`access`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elefant_webpage`
--

LOCK TABLES `elefant_webpage` WRITE;
/*!40000 ALTER TABLE `elefant_webpage` DISABLE KEYS */;
INSERT INTO `elefant_webpage` VALUES ('index','Welcome to Elefant','Home','','public','default','','','<table><tbody><tr><td><h3>Congratulations!</h3>You have successfully installed Elefant, the refreshingly simple new PHP web framework and CMS.</td><td><h3>Getting Started</h3>To log in as an administrator and edit pages, write a blog post, or upload files, go to <a id=\"link-admin\" href=\"/admin\">/admin</a>.</td><td><h3>Developers</h3>Documentation, source code and issue tracking can be found at <a href=\"http://github.com/jbroadway/elefant\">github.com/jbroadway/elefant</a></td></tr></tbody></table>\r\n');
/*!40000 ALTER TABLE `elefant_webpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki`
--

DROP TABLE IF EXISTS `wiki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki` (
  `id` char(72) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki`
--

LOCK TABLES `wiki` WRITE;
/*!40000 ALTER TABLE `wiki` DISABLE KEYS */;
INSERT INTO `wiki` VALUES ('Home','This is the wiki home page');
/*!40000 ALTER TABLE `wiki` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-05-23 14:21:09

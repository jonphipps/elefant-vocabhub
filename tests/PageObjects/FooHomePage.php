<?php
/**
 * Created by jphipps, on 5/24/13 at 10:34 AM
 * for the elefant-vocabhub project
 */

class FooHomePage {
	public static $URL = '';
	public static $adminLink = "/admin";
	public static $title = "Foo Enterprises";
}
<?php
/**
 * Created by jphipps, on 5/23/13 at 7:43 PM
 * for the elefant-vocabhub project
 */

class LoginPage {
	public static $emailInput = "username";
	public static $passwordInput = "password";
	public static $submitButton = "Sign in";
	public static $URL = 'user/login';
	public static $title = "Open Metadata Registry";
}

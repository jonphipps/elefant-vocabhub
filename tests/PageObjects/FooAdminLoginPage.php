<?php
/**
 * Created by jphipps, on 5/23/13 at 7:43 PM
 * for the elefant-vocabhub project
 */

class FooAdminLoginPage extends LoginPage {
	public static $URL = 'user/login';
	public static $title = "Foo Enterprises";
}

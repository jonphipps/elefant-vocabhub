<?php
/**
 * Created by jphipps, on 5/21/13 at 6:14 PM
 * for the elefant-vocabhub project
 */

class SignupPage {
	public static $URL = 'user/signup';
	public static $title = "Sign Up";
	public static $nameInput = "name";
	public static $emailInput = "email";
	public static $passwordInput = "password";
	public static $password2Input = "verify_pass";
	public static $companyInput = "customer-name";
	public static $subdomainInput = "subdomain";
	public static $publicInput = "public";
	public static $submitButton = "input#submitButton";

}
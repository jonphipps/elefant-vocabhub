<?php
/**
 * Created by jphipps, on 5/9/13 at 1:51 PM
 * for the elefant-testinstall project
 */

class UserController {
	protected $user;

	public function __construct(WebGuy $I) {
		$this->user = $I;
	}

	public function loginAdmin($name, $pw) {
		$this->user->amGoingTo("login as admin");
		$this->user->seeInCurrentUrl(AdminLoginPage::$URL);
		$this->user->fillField(AdminLoginPage::$emailInput, $name);
		$this->user->fillField(AdminLoginPage::$passwordInput, $pw);
		$this->user->click(AdminLoginPage::$submitButton);
		$this->user->see(adminUser::$name);
		$this->user->seeElement("div#admin-bar");
	}

	public function loginFooAdmin($name, $pw) {
		$this->user->amGoingTo("login as the foo admin");
		$this->user->seeInCurrentUrl(FooAdminLoginPage::$URL);
		$this->user->fillField(FooAdminLoginPage::$emailInput, $name);
		$this->user->fillField(FooAdminLoginPage::$passwordInput, $pw);
		$this->user->click(FooAdminLoginPage::$submitButton);
		$this->user->see(FooAdmin::$name);
		//$this->user->seeElement("div#admin-bar");
	}

	public function logout() {
		$this->user->amGoingTo("logout");
		$this->user->amOnPage('user/logout');
		$this->user->wait(1000);
	}
}
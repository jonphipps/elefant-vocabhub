<?php
/**
 * Created by jphipps, on 5/22/13 at 9:04 AM
 * for the elefant-vocabhub project
 */

class DbController {
	public static $customerTable = "elefant_saasy_customer";
	public static $accountTable = "elefant_saasy_acct";
	public static $userTable = "elefant_user";

	public static $projectDir = "/Users/jphipps/sites/elefant-vocabhub/";

}
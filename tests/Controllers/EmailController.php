<?php
/**
 * Created by jphipps, on 5/30/13 at 8:55 AM
 * for the elefant-vocabhub project
 */
//ini_set ('include_path', ini_get ('include_path') . ':' . DbController::$projectDir .  'lib/vendor');
//require_once (DbController::$projectDir . 'lib/Functions.php');
//require_once (DbController::$projectDir . 'lib/Autoloader.php');

class EmailController extends \Codeception\Module\Filesystem {

	private $mail;
	protected $verifyLink;
	protected $verifyUrl;
	public $dbUserData;
	public $inviteLogin;
	public $invitePassword;

//	public function __construct(){
//		xdebug_break();
//		$this->mail = new Zend_Mail_Storage_Maildir(array('dirname' =>
//		DbController::$projectDir . "cache/mailer"));
//	}

	public function openLatestMailerFile($path) {
		/** @var Filesystem $filesystem */
		$latest_ctime    = 0;
		$latest_filename = '';

		$d = dir($path);
		while (false !== ($entry = $d->read())) {
			$filepath = "{$path}/{$entry}";
			// could do also other checks than just checking whether the entry is a file
			if (is_file($filepath) && filectime($filepath) > $latest_ctime) {
				$latest_ctime    = filectime($filepath);
				$latest_filename = $entry;
			}
		}
		//xdebug_break();
		$file = $path . DIRECTORY_SEPARATOR . $latest_filename;
		//$filesystem->seeFileFound($file);
		//$foo = file_get_contents($file);
		//print_r($foo);
		$this->filepath = $file;
		$this->openFile($file);
		$this->assertContains("http", $this->file);
		if (preg_match('/verifier=3D(.*)&email/i', $this->file, $matches)) {
			$this->dbUserData = '{"verifier":"' . $matches[1] . '"}';
		}
	}

	public function getVerifyLinkFromEmail() {
		//find the link, plus a bit of surrounding text
		$pattern = array('/(?mi-Us)\\R/', '/(?mi-Us)=\r/', '/=3D/', '|(?mi-Us).*(http://.*)=0A=0AT.*|');
		$replace = array("\r", "", "=" , "$1");
		//strip the newlines and extract the link
		$link = preg_replace($pattern, $replace, $this->file);
		$this->verifyUrl = $link;
		if (preg_match('%http://foo\.vocabhub\.dev/(.*)%i', $link, $matches)) {
			$this->verifyLink = $matches[1];
		}
		return $this->verifyLink;

		/** @var PhpBrowser $browser */
		//$browser = $this->getModule("PhpBrowser");
		//$browser-amOnPage($link);

	}

	public function getInviteCredentialsFromEmail() {
		//find the link, plus a bit of surrounding text
		$pattern = array('/(?mi-Us)\\R/', '/(?mi-Us)=\r/');
		$replace = array("\r", "");
		//strip the newlines and extract the link
		//xdebug_break();
		$clean = preg_replace($pattern, $replace, $this->file);
		if (preg_match('/=0A=0AEmail: (.*)=0APassword: (.*?)=0A=0A/', $clean, $matches)) {
			$this->inviteLogin = $this->decode_entities($matches[1]);
			$this->invitePassword = $this->decode_entities($matches[2]);
		}

		/** @var PhpBrowser $browser */
		//$browser = $this->getModule("PhpBrowser");
		//$browser-amOnPage($link);

	}

	public function deleteLatestMailerFile() {
		$this->deleteFile($this->filepath);
		$this->assertFalse($this->seeFileFound($this->filepath));
	}

	private function decode_entities($text) {
		$text = quoted_printable_decode($text);
		$text= html_entity_decode($text,ENT_QUOTES,"ISO-8859-1"); #NOTE: UTF-8 does not work!
		$text= preg_replace('/&#(\d+);/me',"chr(\\1)",$text); #decimal notation
		$text= preg_replace('/&#x([a-f0-9]+);/mei',"chr(0x\\1)",$text);  #hex notation
		return $text;
	}

}
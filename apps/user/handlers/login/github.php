<?php

/**
 * github social login handler.
 */
use OAuth\OAuth2\Service\GitHub;
use OAuth\Common\Storage\Memory;
use OAuth\Common\Consumer\Credentials;
use Github\Client;

if (! in_array('github', $appconf['User']['login_methods'])) {
	echo $this->error(404, __('Not found'), __('The page you requested could not be found.'));

	return;
}

require_once __DIR__ . '/../../lib/OAuthbootstrap.php';

// In-memory storage
$storage = new Memory();

//redirect URI
$my_url = 'http://' . $_SERVER['HTTP_HOST'] . '/user/login/github?redirect=' . urlencode($_GET['redirect']);

// Setup the credentials for the requests
$credentials =
		new Credentials($servicesCredentials['github']['key'], $servicesCredentials['github']['secret'], $my_url);

xdebug_break();

// Instantiate the GitHub service using the credentials, http client and storage mechanism for the token
/** @var $gitHub GitHub */
$gitHub = $serviceFactory->createService('GitHub', $credentials, $storage, array('user'));

@session_start();

//get the code if we don't have one
$code = $_REQUEST['code'];

if (empty ($code)) {
	$_SESSION['state'] = md5(uniqid(rand(), true)); //CSRF protection

	$url = $gitHub->getAuthorizationUri(array('state' => $_SESSION['state']));

	$page->layout = false;
	echo '<script>top.location.href="' . $url . '";</script>';

	return;
}

//we didn't get the correct 'state' back
if ($_REQUEST['state'] != $_SESSION['state']) {
	$page->title = 'An Error Occurred';
	echo 'Please try again later.';

	return;
}

// This was a callback request from github, get the token
$gitHubToken = $gitHub->requestAccessToken($_GET['code']);
$access_token = $gitHubToken->getAccessToken();

//get the info from github
$client = new Client();
$client->authenticate($access_token, "", $client::AUTH_HTTP_TOKEN);

/** @var $githubUser \Github\Api\CurrentUser */
$githubUser = $client->api('current_user');
$user = $githubUser->show();

//get all of the user's emails from github
$emails = $githubUser->emails()->all();

$token = 'git:' . $user['id'] . ':' . $access_token;
$_SESSION['session_git'] = $token;

$uid = User_OpenID::get_user_id($token);
if ($uid) {
	$u = new User ($uid);
}

if ($u) {
	// already have an account, log them in
	$u->session_id = md5(uniqid(mt_rand(), 1));
	$u->expires    = gmdate('Y-m-d H:i:s', time() + 2592000);
	$try           = 0;
	while (! $u->put()) {
		$u->session_id = md5(uniqid(mt_rand(), 1));
		$try ++;
		if ($try == 5) {
			$this->redirect($_GET['redirect']);
		}
	}
	$_SESSION['session_id'] = $u->session_id;

	// save openid token
	$oid = new User_OpenID (array(
	                             'token'   => $token,
	                             'user_id' => $u->id
	                        ));
	$oid->put();

	$this->redirect($_GET['redirect']);
} else {

	//Check to see if they already signed in with the same email retrieved from github
	//If so link that email and Open_ID
	//try all of the emails
	foreach ($emails as $email) {
		$u = User::query()->where('email', $email)->single();

		if ($u) {
			$oid = new User_OpenID (array(
			                             'token'   => $token,
			                             'user_id' => $u->id
			                        ));
			$oid->put();

			$u->session_id = md5(uniqid(mt_rand(), 1));
			$u->expires    = gmdate('Y-m-d H:i:s', time() + 2592000);
			$u->last_login = gmdate('Y-m-d H:i:s');
			$try           = 0;

			while (! $u->put()) {
				$u->session_id = md5(uniqid(mt_rand(), 1));
				$try ++;
				if ($try == 5) {
					$this->redirect($_GET['redirect']);
				}
			}

			$_SESSION['session_id'] = $u->session_id;

			// save openid token
			$oid = new User_OpenID (array(
			                             'token'   => $token,
			                             'user_id' => $u->id
			                        ));

			$oid->put();
			$this->redirect($_GET['redirect']);
		}
	}

	//Otherwise signup form to create a linked account, prefill name
	$_POST['name']     = $user['name'];
	$_POST['email']    = $user['email'];
	$_POST['redirect'] = $_GET['redirect'];
	$_POST['token']    = $token;
	echo $this->run('user/login/newuser');
}

?>

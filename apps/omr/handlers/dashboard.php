<?php

// Authorize user
if (! saasy\App::authorize ($page, $tpl)) return;

$customer_id = saasy\App::customer ()->id;
$q = omr\Dashboard::query ()->where ('customer', $customer);
$t = omr\Dashboard::query ()->where ('customer', $customer);

$dashboard = $q->fetch_orig (20, 0);
$dashboard = is_array ($dashboard) ? $dashboard : array ();
$total = $t->count ();

echo $tpl->render (
	'omr/dashboard',
	array (
		'dashboard' => $dashboard,
		'total' => $total
	)
);

?>
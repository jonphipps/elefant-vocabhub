<?php

// Authorize user
if (! saasy\App::authorize ($page, $tpl)) return;

$customer_id = saasy\App::customer ()->id;
$q = omr\Vocabs::query ()->where ('customer', $customer);
$t = omr\Vocabs::query ()->where ('customer', $customer);

$vocabs = $q->fetch_orig (20, 0);
$vocabs = is_array ($vocabs) ? $vocabs : array ();
$total = $t->count ();

echo $tpl->render (
	'omr/vocabs',
	array (
		'vocabs' => $vocabs,
		'total' => $total
	)
);

?>
<?php

// Authorize user
if (! saasy\App::authorize ($page, $tpl)) return;

$customer_id = saasy\App::customer ()->id;
$q = omr\ElementSets::query ()->where ('customer', $customer);
$t = omr\ElementSets::query ()->where ('customer', $customer);

$elementsets = $q->fetch_orig (20, 0);
$elementsets = is_array ($elementsets) ? $elementsets : array ();
$total = $t->count ();

echo $tpl->render (
	'omr/elementsets',
	array (
		'elementsets' => $elementsets,
		'total' => $total
	)
);

?>
<?php

// Authorize user
if (! saasy\App::authorize ($page, $tpl)) return;

$customer_id = saasy\App::customer ()->id;
$q = omr\Profiles::query ()->where ('customer', $customer);
$t = omr\Profiles::query ()->where ('customer', $customer);

$profiles = $q->fetch_orig (20, 0);
$profiles = is_array ($profiles) ? $profiles : array ();
$total = $t->count ();

echo $tpl->render (
	'omr/profiles',
	array (
		'profiles' => $profiles,
		'total' => $total
	)
);

?>
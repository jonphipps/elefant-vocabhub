<?php

// Authorize user
if (! saasy\App::authorize ($page, $tpl)) return;

$customer_id = saasy\App::customer ()->id;
$q = omr\Maps::query ()->where ('customer', $customer);
$t = omr\Maps::query ()->where ('customer', $customer);

$maps = $q->fetch_orig (20, 0);
$maps = is_array ($maps) ? $maps : array ();
$total = $t->count ();

echo $tpl->render (
	'omr/maps',
	array (
		'maps' => $maps,
		'total' => $total
	)
);

?>
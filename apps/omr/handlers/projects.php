<?php

// Authorize user
if (! saasy\App::authorize ($page, $tpl)) return;

$customer_id = saasy\App::customer ()->id;
$q = omr\Projects::query ()->where ('customer', $customer);
$t = omr\Projects::query ()->where ('customer', $customer);

$projects = $q->fetch_orig (20, 0);
$projects = is_array ($projects) ? $projects : array ();
$total = $t->count ();

echo $tpl->render (
	'omr/projects',
	array (
		'projects' => $projects,
		'total' => $total
	)
);

?>
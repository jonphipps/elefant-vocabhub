<?php

namespace omr;

use Model;

class Dashboard extends Model {
	public $table = '#prefix#omr_dashboard';
	
	public $fields = array (
		'customer' => array ('belongs_to' => 'saasy\Customer', 'field_name' => 'customer')
	);
}

?>
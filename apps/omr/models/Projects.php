<?php

namespace omr;

use Model;

class Projects extends Model {
	public $table = '#prefix#omr_projects';
	
	public $fields = array (
		'customer' => array ('belongs_to' => 'saasy\Customer', 'field_name' => 'customer')
	);
}

?>
<?php

namespace omr;

use Model;

class Vocabs extends Model {
	public $table = '#prefix#omr_vocabs';
	
	public $fields = array (
		'customer' => array ('belongs_to' => 'saasy\Customer', 'field_name' => 'customer')
	);
}

?>
<?php

namespace omr;

use Model;

class Maps extends Model {
	public $table = '#prefix#omr_maps';
	
	public $fields = array (
		'customer' => array ('belongs_to' => 'saasy\Customer', 'field_name' => 'customer')
	);
}

?>
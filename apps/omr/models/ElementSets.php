<?php

namespace omr;

use Model;

class ElementSets extends Model {
	public $table = '#prefix#omr_elementsets';
	
	public $fields = array (
		'customer' => array ('belongs_to' => 'saasy\Customer', 'field_name' => 'customer')
	);
}

?>
<?php

namespace omr;

use Model;

class Profiles extends Model {
	public $table = '#prefix#omr_profiles';
	
	public $fields = array (
		'customer' => array ('belongs_to' => 'saasy\Customer', 'field_name' => 'customer')
	);
}

?>
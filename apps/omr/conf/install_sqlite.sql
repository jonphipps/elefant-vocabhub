-- for omr\Dashboard model
create table #prefix#omr_dashboard (
	id integer primary key,
	customer int not null
	-- add your fields
);

create index #prefix#omr_dashboard_customer on #prefix#omr_dashboard (customer);

-- for omr\Projects model
create table #prefix#omr_projects (
	id integer primary key,
	customer int not null
	-- add your fields
);

create index #prefix#omr_projects_customer on #prefix#omr_projects (customer);

-- for omr\Vocabs model
create table #prefix#omr_vocabs (
	id integer primary key,
	customer int not null
	-- add your fields
);

create index #prefix#omr_vocabs_customer on #prefix#omr_vocabs (customer);

-- for omr\ElementSets model
create table #prefix#omr_elementsets (
	id integer primary key,
	customer int not null
	-- add your fields
);

create index #prefix#omr_elementsets_customer on #prefix#omr_elementsets (customer);

-- for omr\Profiles model
create table #prefix#omr_profiles (
	id integer primary key,
	customer int not null
	-- add your fields
);

create index #prefix#omr_profiles_customer on #prefix#omr_profiles (customer);

-- for omr\Maps model
create table #prefix#omr_maps (
	id integer primary key,
	customer int not null
	-- add your fields
);

create index #prefix#omr_maps_customer on #prefix#omr_maps (customer);


-- for omr\Dashboard model
create table #prefix#omr_dashboard (
	id int not null auto_increment primary key,
	customer int not null,
	-- add your fields
	index (customer)
);

-- for omr\Projects model
create table #prefix#omr_projects (
	id int not null auto_increment primary key,
	customer int not null,
	-- add your fields
	index (customer)
);

-- for omr\Vocabs model
create table #prefix#omr_vocabs (
	id int not null auto_increment primary key,
	customer int not null,
	-- add your fields
	index (customer)
);

-- for omr\ElementSets model
create table #prefix#omr_elementsets (
	id int not null auto_increment primary key,
	customer int not null,
	-- add your fields
	index (customer)
);

-- for omr\Profiles model
create table #prefix#omr_profiles (
	id int not null auto_increment primary key,
	customer int not null,
	-- add your fields
	index (customer)
);

-- for omr\Maps model
create table #prefix#omr_maps (
	id int not null auto_increment primary key,
	customer int not null,
	-- add your fields
	index (customer)
);

